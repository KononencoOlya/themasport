const commentsSwiperEl = new Swiper(".commentsSwiper", {
    breakpoints: {
      769: {
        pagination: {
          el: ".swiper-pagination_comments",
          type: "fraction",
        },
      }
    },
    slidesPerView: 1,
    pagination: {
      el: ".swiper-pagination_comments",
      type: "bullets",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next_comments",
      prevEl: ".swiper-button-prev_comments"
  }
  });
