const rulesSportsSliderEl = new Swiper(".rulesSportsSlider", {
    spaceBetween: 20,
    initialSlide: 2,
    slidesPerView: "auto",
    breakpoints: {
        0: {
          slidesPerView: 1.2,
          spaceBetween: 15,
  
        },
        769: {
          slidesPerView: 2.5,
          pagination: {
            el: ".swiper-pagination_rules-sports",
            type: "fraction"
          },
        },
      },
      pagination: {
          el: ".swiper-pagination_rules-sports",
          type: "bullets",
          clickable: true,
        },
        navigation: {
          nextEl: ".swiper-button-next_rules-sports",
          prevEl: ".swiper-button-prev_rules-sports"
      },
    });