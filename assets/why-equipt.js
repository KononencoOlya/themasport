document.addEventListener('click', openAccordion);

function openAccordion(ev) {
    let accordionItem = ev.target.closest('.accordion_item');
    if (accordionItem) {
        accordionItem.classList.toggle('accordion_item_active');
    }
}