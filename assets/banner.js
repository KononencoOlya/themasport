const bannerSliderEl = new Swiper(".bannerSlider", {
  spaceBetween: 30,
  breakpoints: {
      0: {
        slidesPerView: 1.2,
        spaceBetween: 15,
        pagination: {
          el: ".swiper-pagination_banner",
          type: "bullets",
          clickable: true,
        },

      },
      576: {
        slidesPerView: 2.5,
        pagination: {
          el: ".swiper-pagination_banner",
          type: "bullets",
          clickable: true,
        },
      },
      769: {
        slidesPerView: 2.5,
      },
      992: {
        slidesPerView: 3.5,
      }
    },
      pagination: {
        el: ".swiper-pagination_banner",
        type: "fraction",
        clickable: true,
      },
      navigation: {
        nextEl: ".swiper-button-next_banner",
        prevEl: ".swiper-button-prev_banner"
    },
  });