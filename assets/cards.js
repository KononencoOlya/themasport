const cardsSliderEl = new Swiper(".cardsSlider", {
    slidesPerView: 1.1,
    spaceBetween: 15,
    breakpoints: {
        768: {
            enabled: false,
            slidesPerView: 'auto',
        }
    },
    pagination: {
      el: ".swiper-pagination_cards",
      type: "bullets",
      clickable: true,      
    }
  });