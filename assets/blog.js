const blogSliderEl = new Swiper(".blogSlider", {
    slidesPerView: 1.3,
    spaceBetween: 15,
    breakpoints: {
        768: {
            enabled: false,
            slidesPerView: 'auto',
        }
    },
    pagination: {
      el: ".swiper-pagination_blog",
      type: "bullets",
      clickable: true,
    }
  });