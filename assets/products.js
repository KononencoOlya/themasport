const productsSwiperEl = new Swiper(".productsSwiper", {
    breakpoints: {
      0: {
        slidesPerView: 1.3,
        spaceBetween: 15,
        pagination: {
          el: ".swiper-pagination_products",
          clickable: true,
        },
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      769: {
        slidesPerView: 4,
        spaceBetween: 20
      },
    },
    navigation: {
      nextEl: ".swiper-button-next_products",
      prevEl: ".swiper-button-prev_products"
  },
  });

